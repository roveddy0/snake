// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MySnake/MySnakeActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMySnakeActor() {}
// Cross Module References
	MYSNAKE_API UClass* Z_Construct_UClass_AMySnakeActor_NoRegister();
	MYSNAKE_API UClass* Z_Construct_UClass_AMySnakeActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MySnake();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	DEFINE_FUNCTION(AMySnakeActor::execMoveSnake)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MoveSnake();
		P_NATIVE_END;
	}
	void AMySnakeActor::StaticRegisterNativesAMySnakeActor()
	{
		UClass* Class = AMySnakeActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "MoveSnake", &AMySnakeActor::execMoveSnake },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMySnakeActor_MoveSnake_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMySnakeActor_MoveSnake_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//??????? ???????? ?????? ?? ???????? ???????????\n" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMySnakeActor_MoveSnake_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMySnakeActor, nullptr, "MoveSnake", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMySnakeActor_MoveSnake_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AMySnakeActor_MoveSnake_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMySnakeActor_MoveSnake()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMySnakeActor_MoveSnake_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMySnakeActor_NoRegister()
	{
		return AMySnakeActor::StaticClass();
	}
	struct Z_Construct_UClass_AMySnakeActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisibleSnakeElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_VisibleSnakeElement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectionMoveSnake_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DirectionMoveSnake;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMySnakeActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MySnake,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMySnakeActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMySnakeActor_MoveSnake, "MoveSnake" }, // 1476635670
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MySnakeActor.h" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleSnakeElement_MetaData[] = {
		{ "Category", "MySnakeActor" },
		{ "Comment", "//??????? ?? ????? ?????? ??????\n" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleSnakeElement = { "VisibleSnakeElement", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySnakeActor, VisibleSnakeElement), METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleSnakeElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleSnakeElement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake_MetaData[] = {
		{ "Category", "MySnakeActor" },
		{ "Comment", "//????????????? ? ????? ??????? ????????? ??????\n" },
		{ "ModuleRelativePath", "MySnakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake = { "DirectionMoveSnake", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AMySnakeActor, DirectionMoveSnake), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AMySnakeActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySnakeActor_Statics::NewProp_VisibleSnakeElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AMySnakeActor_Statics::NewProp_DirectionMoveSnake,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMySnakeActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMySnakeActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMySnakeActor_Statics::ClassParams = {
		&AMySnakeActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AMySnakeActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AMySnakeActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMySnakeActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMySnakeActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMySnakeActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMySnakeActor, 324109420);
	template<> MYSNAKE_API UClass* StaticClass<AMySnakeActor>()
	{
		return AMySnakeActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMySnakeActor(Z_Construct_UClass_AMySnakeActor, &AMySnakeActor::StaticClass, TEXT("/Script/MySnake"), TEXT("AMySnakeActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMySnakeActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
