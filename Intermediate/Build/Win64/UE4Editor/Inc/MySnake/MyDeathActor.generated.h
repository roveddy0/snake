// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSNAKE_MyDeathActor_generated_h
#error "MyDeathActor.generated.h already included, missing '#pragma once' in MyDeathActor.h"
#endif
#define MYSNAKE_MyDeathActor_generated_h

#define MySnake_Source_MySnake_MyDeathActor_h_13_SPARSE_DATA
#define MySnake_Source_MySnake_MyDeathActor_h_13_RPC_WRAPPERS
#define MySnake_Source_MySnake_MyDeathActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define MySnake_Source_MySnake_MyDeathActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyDeathActor(); \
	friend struct Z_Construct_UClass_AMyDeathActor_Statics; \
public: \
	DECLARE_CLASS(AMyDeathActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(AMyDeathActor)


#define MySnake_Source_MySnake_MyDeathActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMyDeathActor(); \
	friend struct Z_Construct_UClass_AMyDeathActor_Statics; \
public: \
	DECLARE_CLASS(AMyDeathActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(AMyDeathActor)


#define MySnake_Source_MySnake_MyDeathActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyDeathActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyDeathActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyDeathActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyDeathActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyDeathActor(AMyDeathActor&&); \
	NO_API AMyDeathActor(const AMyDeathActor&); \
public:


#define MySnake_Source_MySnake_MyDeathActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyDeathActor(AMyDeathActor&&); \
	NO_API AMyDeathActor(const AMyDeathActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyDeathActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyDeathActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyDeathActor)


#define MySnake_Source_MySnake_MyDeathActor_h_13_PRIVATE_PROPERTY_OFFSET
#define MySnake_Source_MySnake_MyDeathActor_h_10_PROLOG
#define MySnake_Source_MySnake_MyDeathActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_MyDeathActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_MyDeathActor_h_13_SPARSE_DATA \
	MySnake_Source_MySnake_MyDeathActor_h_13_RPC_WRAPPERS \
	MySnake_Source_MySnake_MyDeathActor_h_13_INCLASS \
	MySnake_Source_MySnake_MyDeathActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySnake_Source_MySnake_MyDeathActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_MyDeathActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_MyDeathActor_h_13_SPARSE_DATA \
	MySnake_Source_MySnake_MyDeathActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MySnake_Source_MySnake_MyDeathActor_h_13_INCLASS_NO_PURE_DECLS \
	MySnake_Source_MySnake_MyDeathActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSNAKE_API UClass* StaticClass<class AMyDeathActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySnake_Source_MySnake_MyDeathActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
