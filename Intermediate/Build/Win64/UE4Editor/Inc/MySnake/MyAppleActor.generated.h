// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSNAKE_MyAppleActor_generated_h
#error "MyAppleActor.generated.h already included, missing '#pragma once' in MyAppleActor.h"
#endif
#define MYSNAKE_MyAppleActor_generated_h

#define MySnake_Source_MySnake_MyAppleActor_h_13_SPARSE_DATA
#define MySnake_Source_MySnake_MyAppleActor_h_13_RPC_WRAPPERS
#define MySnake_Source_MySnake_MyAppleActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define MySnake_Source_MySnake_MyAppleActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyAppleActor(); \
	friend struct Z_Construct_UClass_AMyAppleActor_Statics; \
public: \
	DECLARE_CLASS(AMyAppleActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(AMyAppleActor)


#define MySnake_Source_MySnake_MyAppleActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMyAppleActor(); \
	friend struct Z_Construct_UClass_AMyAppleActor_Statics; \
public: \
	DECLARE_CLASS(AMyAppleActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(AMyAppleActor)


#define MySnake_Source_MySnake_MyAppleActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyAppleActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyAppleActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyAppleActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyAppleActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyAppleActor(AMyAppleActor&&); \
	NO_API AMyAppleActor(const AMyAppleActor&); \
public:


#define MySnake_Source_MySnake_MyAppleActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyAppleActor(AMyAppleActor&&); \
	NO_API AMyAppleActor(const AMyAppleActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyAppleActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyAppleActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyAppleActor)


#define MySnake_Source_MySnake_MyAppleActor_h_13_PRIVATE_PROPERTY_OFFSET
#define MySnake_Source_MySnake_MyAppleActor_h_10_PROLOG
#define MySnake_Source_MySnake_MyAppleActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_MyAppleActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_MyAppleActor_h_13_SPARSE_DATA \
	MySnake_Source_MySnake_MyAppleActor_h_13_RPC_WRAPPERS \
	MySnake_Source_MySnake_MyAppleActor_h_13_INCLASS \
	MySnake_Source_MySnake_MyAppleActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySnake_Source_MySnake_MyAppleActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_MyAppleActor_h_13_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_MyAppleActor_h_13_SPARSE_DATA \
	MySnake_Source_MySnake_MyAppleActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MySnake_Source_MySnake_MyAppleActor_h_13_INCLASS_NO_PURE_DECLS \
	MySnake_Source_MySnake_MyAppleActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSNAKE_API UClass* StaticClass<class AMyAppleActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySnake_Source_MySnake_MyAppleActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
