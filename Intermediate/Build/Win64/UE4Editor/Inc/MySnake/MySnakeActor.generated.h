// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYSNAKE_MySnakeActor_generated_h
#error "MySnakeActor.generated.h already included, missing '#pragma once' in MySnakeActor.h"
#endif
#define MYSNAKE_MySnakeActor_generated_h

#define MySnake_Source_MySnake_MySnakeActor_h_12_SPARSE_DATA
#define MySnake_Source_MySnake_MySnakeActor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMoveSnake);


#define MySnake_Source_MySnake_MySnakeActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMoveSnake);


#define MySnake_Source_MySnake_MySnakeActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMySnakeActor(); \
	friend struct Z_Construct_UClass_AMySnakeActor_Statics; \
public: \
	DECLARE_CLASS(AMySnakeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(AMySnakeActor)


#define MySnake_Source_MySnake_MySnakeActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMySnakeActor(); \
	friend struct Z_Construct_UClass_AMySnakeActor_Statics; \
public: \
	DECLARE_CLASS(AMySnakeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnake"), NO_API) \
	DECLARE_SERIALIZER(AMySnakeActor)


#define MySnake_Source_MySnake_MySnakeActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMySnakeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMySnakeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySnakeActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySnakeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySnakeActor(AMySnakeActor&&); \
	NO_API AMySnakeActor(const AMySnakeActor&); \
public:


#define MySnake_Source_MySnake_MySnakeActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMySnakeActor(AMySnakeActor&&); \
	NO_API AMySnakeActor(const AMySnakeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMySnakeActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMySnakeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMySnakeActor)


#define MySnake_Source_MySnake_MySnakeActor_h_12_PRIVATE_PROPERTY_OFFSET
#define MySnake_Source_MySnake_MySnakeActor_h_9_PROLOG
#define MySnake_Source_MySnake_MySnakeActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_MySnakeActor_h_12_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_MySnakeActor_h_12_SPARSE_DATA \
	MySnake_Source_MySnake_MySnakeActor_h_12_RPC_WRAPPERS \
	MySnake_Source_MySnake_MySnakeActor_h_12_INCLASS \
	MySnake_Source_MySnake_MySnakeActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySnake_Source_MySnake_MySnakeActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnake_Source_MySnake_MySnakeActor_h_12_PRIVATE_PROPERTY_OFFSET \
	MySnake_Source_MySnake_MySnakeActor_h_12_SPARSE_DATA \
	MySnake_Source_MySnake_MySnakeActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MySnake_Source_MySnake_MySnakeActor_h_12_INCLASS_NO_PURE_DECLS \
	MySnake_Source_MySnake_MySnakeActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSNAKE_API UClass* StaticClass<class AMySnakeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySnake_Source_MySnake_MySnakeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
