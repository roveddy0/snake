// Fill out your copyright notice in the Description page of Project Settings.

#include "MySnake.h"
#include "MyDeathActor.h"

// Sets default values
AMyDeathActor::AMyDeathActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//������������� ��� ��� ���
	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;

	WallColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterials/KillZone_Inst.KillZone_Inst'")).Get();

	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootComponent");
	RootComponent = MyRootComponent;

	class UStaticMeshComponent* WallChank;
	WallChank = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WALL"));
	WallChank->SetStaticMesh(WallMesh);
	WallChank->SetRelativeLocation(FVector(0, 0, 0));
	WallChank->SetMaterial(0, WallColor);

	WallChank->AttachTo(MyRootComponent);


}

// Called when the game starts or when spawned
void AMyDeathActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDeathActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//�������� ������ ���� �����, ������� �������. ������ ��������� ����.
	KillVolume();

}

void AMyDeathActor::KillVolume()
{
	//������, ������� ������ � ������ �������� ��������� ������������
	TArray<AActor*> OverlappedActors;

	//� ��� ���������� �����?
	GetOverlappingActors(OverlappedActors);

	//���������� ���� �������
	for (int32 i = 0; i < OverlappedActors.Num(); ++i)
	{
		//������� �������, � �������� �����������
		OverlappedActors[i]->Destroy(true, true);
	}
}

