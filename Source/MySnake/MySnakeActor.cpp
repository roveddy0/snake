// Fill out your copyright notice in the Description page of Project Settings.

#include "MySnake.h"
#include "MySnakeActor.h"



// Sets default values
AMySnakeActor::AMySnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//������ �������� ���������
	MyRootComponent = CreateAbstractDefaultSubobject<USphereComponent>("MyRoot");
	RootComponent = MyRootComponent;

	//�������� ���������� ������
	FVector Position = GetActorLocation();

	//������ ������������ ������ �� �����
	MyRootComponent->SetRelativeLocation(Position);

	//������ ������ ��������� ������� ����� �� h �����
	CreateSnakeBody();

}

// Called when the game starts or when spawned
void AMySnakeActor::BeginPlay()
{
Super::BeginPlay();

}

// Called every frame
void AMySnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//�������� ������� ��������� ��������� ������ ������ ���� (������ ������ ����)
	SetElementVisible();
	//����� ������ "�� �������" � ������. ������������� ���������� �������� � ��������� ������� �������� ������ (������ ������ ����)
	BufferTime += DeltaTime;
	if (BufferTime > StepDelay)
	{
	  MoveSnake();
	  BufferTime = 0;
	}
}

//������� �������� ���� ������
void AMySnakeActor::CreateSnakeBody()
{
	//������� ������ ����� ��� ������������ (�����)
	class UStaticMesh* SnakeElementMesh;
	SnakeElementMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	//������������� ��� ������ �������� ����
	class UMaterialInstance* BodyColor;
	BodyColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("//MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterials/SnakeBodyMaterial_Inst.SnakeBodyMaterial_Inst'")).Get();

	//MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterials/SnakeBodyMaterial_Inst.SnakeBodyMaterial_Inst' �������� ����

	//������������� �������� ��� ������
	class UMaterialInstance* SnakeHead;
	SnakeHead = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterials/SnakeHeadMaterial_Inst.SnakeHeadMaterial_Inst'")).Get();

	//MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterials/SnakeHeadMaterial_Inst.SnakeHeadMaterial_Inst' - �������� ��� ������

	//���������� ��� �������� ��������� ������
	FVector NextPosition = GetActorLocation();

	//������ ���������� ����� ��� ���� ����� �������� ������. ��������, (�������1, �������2 � �.�.)
	FName ElementName;
	FString BindElementsNames;

	//���������� ������ ������� ������ �� ����� � ������
	for (int32 i = 0; i < SnakeSize; i++)
	{
		BindElementsNames = "Element" + FString::FromInt(i);
		ElementName = FName(*BindElementsNames);

		//������ �������� ������ (����)
		class UStaticMeshComponent* SnakeElementBody = CreateDefaultSubobject<UStaticMeshComponent>(ElementName);
		SnakeElementBody->SetStaticMesh(SnakeElementMesh);

		//����� ���������� ����� ���� ������������ �������� �����
		SnakeElementBody->SetRelativeLocation(NextPosition);

		//��������� �������� ������, ������� �� ��������� � h �����
		SnakeBody.Add(SnakeElementBody);

		//�������� �� �����������
		NextPosition.X -= StepSnake;

		//��������� ��� ������� �� ������ � ������ ������ � ������������� ���� ��������� ������
		SnakeElementBody->AttachTo(MyRootComponent);
		SnakeElementBody->SetMaterial(0, BodyColor);

		//���� ���� ������ (����) �������� ������� - ������ �������� ������ ����
		if (i == 0)
		{
			SnakeElementBody->SetMaterial(0, SnakeHead);

		}
		//���� ������� �� ��������� � �������� ������� (��� ������), �� � ��������� ��������� ��������� ��������.
		else
		{
			SnakeElementBody->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
	}
	//�������� ������� ������ ��������� ������ (������ ������ ����)
	SetElementVisible();
}

void AMySnakeActor::SetElementVisible()
{
	//���������� ����� �������. ���� ������� ������� ������ ������� �������� (h ����), �� �� �������. ���� ��� - �� ���������
	for (int32 IndexShow = 0; IndexShow < SnakeBody.Num(); IndexShow++)
	{
		if (IndexShow < VisibleSnakeElement)
		{
			SnakeBody[IndexShow]->SetVisibility(true, true);
		}
		else
		{
			SnakeBody[IndexShow]->SetVisibility(false, true);
		}
	}
}

void AMySnakeActor::MoveSnake()
{
	//��������� �������� �� �� ������
	if ((DirectionMoveSnake.X != 0) || (DirectionMoveSnake.Y != 0))
	 {
		//������� ����� ������
		for (int BodyIndex = SnakeBody.Num() - 1; BodyIndex > 0; BodyIndex--)
		{

			//�������� ��������� ���������� ����� ������
		
			FVector LastCoodrinates = SnakeBody[BodyIndex - 1]->GetRelativeLocation;
			//����� ����� ������� ��� ������ ����� ��������
			SnakeBody[BodyIndex]->SetRelativeLocation(LastCoodrinates);
		}
			//������������� ��������� ����� ��� ������ 0 ������ ������� (������)			
			FVector StartSnakePosition = SnakeBody[0]->GetRelativeLocation;				

			//� ����� ������� ���������
			if (DirectionMoveSnake.X > 0) StartSnakePosition.X -= StepSnake;
			if (DirectionMoveSnake.X < 0) StartSnakePosition.X += StepSnake;
			if (DirectionMoveSnake.Y > 0) StartSnakePosition.Y += StepSnake;
			if (DirectionMoveSnake.Y < 0) StartSnakePosition.Y -= StepSnake;

			//������������� ���� ������ � ����� �������
			SnakeBody[0]->SetRelativeLocation(StartSnakePosition);
		 }
}


