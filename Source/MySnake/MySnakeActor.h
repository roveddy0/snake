// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "MySnakeActor.generated.h"

UCLASS()
class MYSNAKE_API AMySnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMySnakeActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//������������ ���������� ��������� ������ ��� ������
	int32 SnakeSize = 15;

	//������ ���� ����� ���������� ������
	float StepSnake = 45.f;		

	//������ ������, ��� �������� � ��������
	TArray<UStaticMeshComponent*> SnakeBody;

	//�������� ������� ������
	class USphereComponent* MyRootComponent;

	//������� �������� ���� ������
	void CreateSnakeBody();

	//������� �� ����� ������ ������
	UPROPERTY(EditAnywhere)
	int32 VisibleSnakeElement = 3;

	void SetElementVisible();

	//������������� � ����� ������� ��������� ������
	UPROPERTY(EditAnywhere)
		FVector2D DirectionMoveSnake;

	//�������� ������ ��� ��������
	float StepDelay = 0.5f;
	//�����, ��� ���������� �������� ������ ����� ������ ���
	float BufferTime = 0;

	//������� �������� ������ �� �������� �����������
	UFUNCTION()
	void MoveSnake();
	
};
