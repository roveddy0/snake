// Fill out your copyright notice in the Description page of Project Settings.

#include "MySnake.h"
#include "MyPawnCamera.h"
#include "MyAppleActor.h"
#include "MySnakeActor.h"

// Sets default values
AMyPawnCamera::AMyPawnCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//������ �������� ���������
	MyRootComponent = CreateDefaultSubobject<UBoxComponent>("RootModel");
	RootComponent = MyRootComponent;

	//������ springarm(������) ��� ������ � ���������� �������
	CameraSpring = CreateDefaultSubobject <USpringArmComponent>("SpringArm");
	CameraSpring->SetRelativeLocation(FVector(0, 0, 0));

	//����������� ������ � ������
	CameraSpring->AttachTo(MyRootComponent);

	//������ ���� ������
	MyCamera = CreateDefaultSubobject<UCameraComponent>("Camera");

	//����������� ������ � ������� (� ������)
	MyCamera->AttachTo(CameraSpring, USpringArmComponent::SocketName);

	//����������� ������� ������� �� ������� �����
	CameraSpring->SetRelativeRotation(FRotator(-90.f, 0, 0));

	//���������� ������� �� ����� ������� �����
	CameraSpring->TargetArmLength = 1000.f;

	//��������� �������� � ������� (����� �� ����������� � ������� ���������)
	CameraSpring->bDoCollisionTest = false;
	
	//��������� ������� � �������� 0 (���� ������)
	AutoPossessPlayer = EAutoReceiveInput::Player0;

}

// Called when the game starts or when spawned
void AMyPawnCamera::BeginPlay()
{
	Super::BeginPlay();

	//��������� ������� ������ ��� ������ ���� (������ - ����)
	AddSnakeToMap();
	
}

// Called every frame
void AMyPawnCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//�������� ������ ������� � ���� ������ ����� (1 �������), �� ������ � ��������� ����� (������ ��������� ����)
	BufferTime += DeltaTime;
	if(BufferTime > StepDelay)
	{
		AddRandomApple();
		BufferTime = 0; //�������� ����� � �������� ����� ����� �����
	}
}

// Called to bind functionality to input
void AMyPawnCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//����������� ������� �� ������ (������� ������ � ������ � Input). �������� ������� �� ������ ����� (this)
	InputComponent->BindAxis("KeyMapMove", this, &AMyPawnCamera::FMove);

}

void AMyPawnCamera::AddSnakeToMap()
{
	//�������� ���������� ����� ��������� ����� (playerstart) � �������
	FVector StartPoint = GetActorLocation();
	FRotator StartPointRotation = GetActorRotation();

	//���� ���� ��������, �� ������ ������ � ��������� �����
	
	if (GetWorld())
	{
		MySnakePlayer = GetWorld()->SpawnActor<AMySnakeActor>(StartPoint, StartPointRotation);
	}
}

void AMyPawnCamera::FMove(float ButtonValue) {
	int32 Key = ButtonValue;

	//������ �������� ������
	//FVector2D Movement;

	//�������� ��� ����� ����� � ������� ����-����. ���� �� ����� ����� � ��������������� �������, �� ������ "�� ����" ������
	switch (Key)
	{
	case 1:
		if (Movement.X != 1) {

			Movement = FVector2D(0, 0);
			Movement.X = -1;
		}
		break;

	case 2:
		if (Movement.X != -1) {
			Movement = FVector2D(0, 0);
			Movement.X = 1;
	}
		break;

	case 3:
		if (Movement.Y != -1) {
			Movement = FVector2D(0, 0);
			Movement.Y = 1;
		}
		break;

	case 4:
		if (Movement.Y != -1) {
			Movement = FVector2D(0, 0);
			Movement.Y = -1;
		}
		break;

	}

	//���� ������ ������, �� �� � �������
	if (MySnakePlayer)
	{
		MySnakePlayer->DirectionMoveSnake = Movement;
	}

}

void AMyPawnCamera::AddRandomApple()
{
	FRotator StartPointRotation = FRotator(0, 0, 0);

	//����� � ��������� �����
	float SpawnX = FMath::FRandRange(MinX, MaxX);
	float SpawnY = FMath::FRandRange(MinY, MaxX);
	FVector StartPoint = FVector(SpawnX, SpawnY, SpawnZ);

	//���������� �� ����� � ���. ��� �������� �����, ����� ������ �� ����������� ����� ����� ���� ��� ����� ������� �� �������
	if (MySnakePlayer)
	{
		if (GetWorld())
		{
			GetWorld()->SpawnActor<AMyAppleActor>(StartPoint, StartPointRotation);
		}
	}
}


