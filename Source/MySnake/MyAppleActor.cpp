// Fill out your copyright notice in the Description page of Project Settings.

#include "MySnake.h"
#include "MySnakeActor.h"
#include "MyAppleActor.h"


// Sets default values
AMyAppleActor::AMyAppleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//������ �������� ���������
	MyRootComponent = CreateDefaultSubobject<USphereComponent>("RootFood");
	RootComponent = MyRootComponent;

	//������ ���
	SnakeFood = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Sphere")).Object;

	class UMaterialInstance* FoodColor;
	
	FoodColor = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/SnakeContent/Materials/InstanceMaterials/Food_Material.Food_Material'")).Get();

	//������ ���
	FVector FoodSize = FVector(0.5f, 0.5f, 0.5f);

	//�������� ��� ����� ���
	class UStaticMeshComponent* Food = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Eat"));
	
	//������������� ��� ��� ������ ������
	Food->SetStaticMesh(SnakeFood);

	Food->SetRelativeScale3D(FoodSize);
	Food->SetRelativeLocation(FVector(0, 0, 0));
	Food->SetMaterial(0, FoodColor);
	Food->AttachTo(MyRootComponent);
}

// Called when the game starts or when spawned
void AMyAppleActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyAppleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//�������� ������� ����������� � ���� ������ ���� (������ ����)
	OverlappedFood();

}

void AMyAppleActor::OverlappedFood()
{
	//������ ������ ���, ������� ������ �����
	TArray<AActor*> OverlappedActors;

	//��������� �������, � �������� �����������
	GetOverlappingActors(OverlappedActors);

	for (int32 i = 0; i < OverlappedActors.Num(); ++i)
	{
		AMySnakeActor* const IncreaseSnakeBody = Cast<AMySnakeActor>(OverlappedActors[i]);

		//���� ������ ����������� � ����, �� ����������� �������� � ����
		if (IncreaseSnakeBody)
		{
			IncreaseSnakeBody->VisibleSnakeElement++;

			//������� ���
			Destroy(true, true);
			break; // ��������� �����
		}
	}
}

