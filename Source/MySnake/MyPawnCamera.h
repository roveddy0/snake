// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "MyPawnCamera.generated.h"

UCLASS()
class MYSNAKE_API AMyPawnCamera : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawnCamera();

public:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
	class UBoxComponent* MyRootComponent;

	UPROPERTY(EditAnywhere)
	class USpringArmComponent* CameraSpring;

	UPROPERTY(EditAnywhere)
	class UCameraComponent* MyCamera;

	class AMySnakeActor* MySnakePlayer;

	//������� ���������� ������ �� �����
	void AddSnakeToMap();

	//������� ��������� �������� ������ (scale)
	void FMove(float ButtonVolume);

	//����������� �����
	UPROPERTY(EditAnywhere)
	FVector2D Movement;	

	//��������� ���������� � ����� �������� ������ ����� ����������
	float MinY = -400.f;
	float MaxY = 400.f;
	float MinX = -400.f;
	float MaxX = 400.f;

		//�� ����� ������ ����� ����������� ������ 
	float SpawnZ = 50.f;

	//������� ������ ������
	void AddRandomApple();

	//�������� ����� ������� ���
	float StepDelay = 5.f;
	float BufferTime = 0;

};
